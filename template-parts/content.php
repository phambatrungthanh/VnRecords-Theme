<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VnRecords
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("card");?>>
	<div class="card-image">
		<?php vnrecords_post_thumbnail(); ?>
	</div>
	<div class="card-content <?php if ( is_home() && is_front_page() ) : ?> truncated <?php endif; ?>">
		<header class="entry-header">
			<p class="entry-meta">
				<?php
				vnrecords_posted_in();
				vnrecords_posted_on();
				?>
			</p><!-- .entry-meta -->
			<?php
			if ( is_singular() ) :
				the_title( '<p class="title is-1">', '</h2>' );
			else :
				the_title( '<p class="title is-2"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></p>' );
			endif;
			?>
		</header>
		<div class="content">
			<?php if ( is_home() && is_front_page() ) : ?>
				<?php the_excerpt(); ?>
			<?php else: ?>
				<?php the_content(); ?>
			<?php endif; ?>
		</div>
	</div>
	<footer class="entry-footer">
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
