<?php
/**
 * Template part for displaying promotions
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VnRecords
 */

?>

<article id="promotions-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="article-content">
        <a href="<?php echo  get_permalink()  ?>" class="card is-big">
			<div class="thumbnail">
                <?php the_post_thumbnail(); ?>
            </div>

            <h4 class="card-title">
                <?php the_title(); ?>
            </h4>
        </a>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
