<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VnRecords
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("the-card");?>>
	<div class="the-card-content">
        
        <a href="<?php echo esc_url( get_permalink() )?>">
            <p class="card-image"><?php the_post_thumbnail(); ?></p>
            <p><b><?php the_title(); ?></b></p>
            <p><?php the_excerpt(); ?></p>
        </a>
        <div class="meta">
            <?php vnrecords_posted_on(); ?>
        </div>
        
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
