<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VnRecords
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Muli:400,700&subset=vietnamese' >
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="master-header" class="level">
		<div class="level-left">
			<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src=<?php assets("images/logo-header.png")?> alt="" class="logo"></a>
			</div><!-- .site-branding -->
		</div>
		<nav class="level-item primary-menu-navigation" role="site-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav>
		<div class="level-right"></div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
