<?php
/**
 * VnRecords functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package VnRecords
 */

if ( ! function_exists( 'vnrecords_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function vnrecords_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on VnRecords, use a find and replace
		 * to change 'vnrecords' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'vnrecords', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'vnrecords' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'vnrecords_setup' );


// Our custom post type function
function vnrecords_promotions_postype() {
 
	register_taxonomy('promotions',array('promotions'), array(
		'hierarchical' => true,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,

	  ));
    register_post_type( 'promotions',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Khuyến mãi' ),
				'singular_name' => __( 'Promotion' ),
				'all_items' => __( 'Xem tất cả', 'twentythirteen' ),
				'view_item' => __( 'Xem khuyễn mãi', 'twentythirteen' ),
				'add_new_item' => __( 'Thêm khuyến mãi', 'twentythirteen' ),
				'add_new' => __( 'Thêm', 'twentythirteen' ),
				'edit_item' => __( 'Sửa', 'twentythirteen' ),
				'update_item' => __( 'Cập nhật', 'twentythirteen' ),
			),
			'supports' => array( 'title', 'editor', 'thumbnail',),
			'taxonomies' => array( 'promotions' ),
            'public' => true,
            'has_archive' => true,
        )
	);

}
// Hooking up our function to theme setup
add_action( 'init', 'vnrecords_promotions_postype' );

// Setup vnrecords version of custom-reladed-post
if (class_exists("CustomRelatedPosts")) {
	add_action("after_setup_theme", "vnrecords_modify_custom_related_post_plugin");
	function vnrecords_save_related_posts_with_thumbnail($id, $post) {
        $update_post_post_type = $post->post_type;
        $post_types = CustomRelatedPosts::option( 'general_post_types', array( 'post', 'page' ) );

        if( in_array( $update_post_post_type, $post_types ) ) {
            $updated_data = array(
				'id' => $post->ID,
				'title' => $post->post_title,
				'permalink' => get_permalink( $post ),
				'status' => $post->post_status,
				'date' => $post->post_date,
				"thumbnail_url" => get_the_post_thumbnail_url($post),
			);

            $relations_from = CustomRelatedPosts::get()->relations_from( $id );
            $relations_to = CustomRelatedPosts::get()->relations_to( $id );
            $relation_ids = array_unique( array_merge( array_keys( $relations_to ), array_keys( $relations_from ) ) );

            foreach( $relation_ids as $relation_id ) {
                $target_relations_from = CustomRelatedPosts::get()->relations_from( $relation_id );
                $target_relations_to = CustomRelatedPosts::get()->relations_to( $relation_id );

                if( array_key_exists( $id, $target_relations_from ) ) {
                    $target_relations_from[$id] = $updated_data;
                    CustomRelatedPosts::get()->helper( 'relations' )->update_from( $relation_id, $target_relations_from );
                }
                if( array_key_exists( $id, $target_relations_to ) ) {
                    $target_relations_to[$id] = $updated_data;
                    CustomRelatedPosts::get()->helper( 'relations' )->update_to( $relation_id, $target_relations_to );
                }
            }
        }
	}
	function vnrecords_modify_custom_related_post_plugin() {
		$result = remove_action("save_post", array(CustomRelatedPosts::get()->helper("cache"), "updated_post"), 11);
		if ($result) {
			add_action("save_post", "vnrecords_save_related_posts_with_thumbnail", 11, 2);
		}
	}
}
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vnrecords_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'vnrecords_content_width', 640 );
}
add_action( 'after_setup_theme', 'vnrecords_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vnrecords_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Home Sidebar', 'vnrecords' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'vnrecords' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="heading"><h4 class="widget-title"><span>',
		'after_title'   => '</span></h4></div>',
	) );
}
add_action( 'widgets_init', 'vnrecords_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vnrecords_scripts() {
	wp_enqueue_style( 'vnrecords-style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'vnrecords_scripts' );


/**
 * Limit posts per page
 */

function vnrecords_limit_posts_per_page($query) {

	if (is_category()) {
		$query->set('posts_per_page', 1);
	}
}
	
//this adds the function above to the 'pre_get_posts' action     
add_action('pre_get_posts', 'vnrecords_limit_posts_per_page');


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function vnrecords_custom_excerpt_length( $length ) {
    return 100;
}
add_filter( 'excerpt_length', 'vnrecords_custom_excerpt_length', 999 );


/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function vnrecords_excerpt_more( $more ) {
    return '...</p><p><a class="moretag" href="'. get_permalink(get_the_ID()) . '">Đọc tiếp...</a>';
}
add_filter( 'excerpt_more', 'vnrecords_excerpt_more' );


require get_template_directory() . '/includes/template-helpers.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/includes/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/includes/jetpack.php';
}

