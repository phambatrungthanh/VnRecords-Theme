<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package VnRecords
 */

get_header();
?>
    <div class="page-separator"></div>
	<?php while ( have_posts() ) : the_post(); ?>
    <div class="container">
        <article id="post-<?php the_ID(); ?>" <?php post_class("card");?>>
            <div class="card-image">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="card-content">
                <div class="columns">
                    <div class="column is-10">
                        <header class="entry-header">
                            <p class="entry-meta">
                                <?php
                                vnrecords_posted_in();
                                vnrecords_posted_on();
                                ?>
                            </p><!-- .entry-meta -->
                            <?php
                                the_title( '<p class="title is-1"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></p>' );
                            ?>
                        </header>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                        
                    </div>
                    <div class="column is-2">Share</div>
                </div>
            </div>
            <footer class="entry-footer">
            </footer><!-- .entry-footer -->

        </article><!-- #post-<?php the_ID(); ?> -->
    </div>


    <?php endwhile; // End of the loop. ?>
<?php
get_footer();
