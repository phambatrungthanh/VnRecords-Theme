const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: [
      './src/js/index.js',
      './src/scss/app.scss'
    ],
  output: {
    path: path.resolve(__dirname, ''),
    filename: 'app.js'
  },
  module: {
    rules: [{
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          'css-loader',
          'sass-loader'
        ]
      })
    },
    {
      test: /\.(png|jpg|jpeg|gif)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: "[name].[ext]",
            outputPath: "assets/images"
          }
        }
      ]
    },
    {
    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
    use: [{
        loader: 'file-loader',
        options: {
            name: '[name].[ext]',
            outputPath: 'assets/fonts/'
        }
      }]
    }]
  },
  plugins: [
    new ExtractTextPlugin('style.css'),
  ]
};