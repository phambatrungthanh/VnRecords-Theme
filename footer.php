<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VnRecords
 */

?>

	</div><!-- #content -->
	<section role="contact" class="">
		<div class="contact-content content-padding">
			<div class="contact-us-icon">
				<span class="icon"><i class="fi flaticon-send"></i></span>
			</div>
			<h2 class="has-text-centered">Liên hệ với chúng tôi</h2>
		</div>
	</section>
	<footer id="colophon" class="site-footer">
		<div class="footer-navigation"></div>
		<div class="site-info">
			<p class="has-text-centered">
				<img src="<?php assets("images/logo-header.png")?>" alt="" class="logo" style="height: 60px;">

			</p>
			<p class="has-text-centered copyrights">
			Copyright © 2018. All Rights Reserved. 
			</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
