<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VnRecords
 */

get_header();
?>

	<div id="category_archiver" class="content-area">
		<main id="main" class="site-main">
            <header class="big-header is-narrow">
                <div class="the-header-feature-image">
                    <img src="<?php echo vnrecords_taxonomy_backgroud_image(); ?>" />
                </div>
                <div class="header-content-centered">
                    <div class="header-content">
                        <h1 class="header-title display-3"><?php single_cat_title(); ?></h1>
                        <h3><?php the_archive_description(); ?></h3>
                    </div>

                </div>

            </header>
            <section class="content-padding">
                <header class="section-title">
                    <h1>Bài viết mới nhất</h1>
                </header>
                <?php if ( have_posts() ) : ?>
                    <div class="category-archive columns is-multiline">
                        
                        <?php while ( have_posts() ) : ?>
                            <div class="column is-one-third">   
                                <?php the_post(); ?>
                                <?php get_template_part( 'template-parts/content-card'); ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php echo vnrecords_pagination(); ?>


<?php
get_footer();
