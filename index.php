<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VnRecords
 */

get_header();
?>
	<section id="frontpage-slider">
	 <?php
		echo do_shortcode('[smartslider3 alias="home-slider"]');
		?>
	</section>
	<section id="tournaments">
		<div class="columns is-gapless">
			<div class="column is-one-third">
				<div class="responsive-box">
					<div class="responsive-box-content tournament-box">
						<div class="heading tournament-heading">
							<h4><span class="heading-icon"><i class="fi flaticon-trophy"></i></span><span>Tournament</span></h3>
						</div>	
						<div class="tournament-content">
							<img src="<?php assets("images/contest/2.jpg") ?>" alt="" srcset="">
						</div>
					</div>
				</div>
			</div>
			<div class="column is-one-third">
				<div class="responsive-box">
					<div class="responsive-box-content tournament-box">

					</div>
				</div>
			</div>
			<div class="column is-one-third">
				<div class="responsive-box">
					<div class="responsive-box-content tournament-box">
						<div class="heading tournament-heading">
							<h4><span class="heading-icon"><i class="fi flaticon-sponsor"></i></span><span>Sponsor Contest</span></h4>
						</div>	
						<div class="tournament-content">
							<img src="<?php assets("images/contest/1.jpg") ?>" alt="" srcset="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="columns">
			<div class="column is-8">
				<div class="page-separator"></div>
				<div class="heading-wrapper">
					<div class="heading">
						<p class="title is-2"><span class="heading-icon"><i class="fi flaticon-note"></i></span><span>Tin tức</span></p>
					</div>				
				</div>

				<section id="main" class="site-main">
					<?php
					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) :
							?>
							<header>
								<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
							</header>
							<?php
						endif;

						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							* Include the Post-Type-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Type name) and that will be used instead.
							*/
							get_template_part( 'template-parts/content', get_post_type() );

						endwhile;

						the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>

				</section><!-- #main -->
			</div>
			<div class="column is-4 sidebar-container">
				<div class="page-separator"></div>
				<div class="sidebar">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>

	<div id="the-post-navigation">
		<?php the_posts_navigation(); ?>
	</div>
	<section id="promotions" class="content-padding">
		<header>
			<h2>Tin khuyến mãi</h2>
		</header>
		<div class="promotions-content">
			<?php 
				query_posts(array( 
					'post_type' => 'promotions',
					'showposts' => 4
				) );
			?>
			<div class="columns">
				<?php while (have_posts()) : the_post(); ?>
					<div class="column is-3">
						<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
					</div>
				<?php endwhile;?>
			</div>
		</div>
		<footer>
			<p class="has-text-centered">
				<a href="#" class="button is-white">Xem tất cả</a>
			</p>
		</footer>
	</section>
<?php
get_footer();
